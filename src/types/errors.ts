export class ValidationError extends Error {
    constructor(message?: string) {
        super(message)
    }
}

export function errorHandler (err: any, req: any, res: any, next: any) {
    console.error(err)
    if (err instanceof ValidationError) {
        res.send({ ok: false, result: err.message })
    } else {
        res.status(500)
        res.send({ok: false, result: '500 Internal Server Error'})
    }
}