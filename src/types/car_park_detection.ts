export interface ISpaceInfo {
    space_id: number
    license_plate: string
    occupied: boolean
    channel: number
    facility_id?: number
    level_id?: number
}

