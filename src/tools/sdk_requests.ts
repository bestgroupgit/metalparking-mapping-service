import axios from 'axios'
import { SDK_ENDPOINT, NVR_ID } from '../config/keys'

const LEDApi = (channel: number | string) => `/parking/api/nvr/${NVR_ID}/channel/${channel}`

export const changeLED = async (channel: number | string, color: string, flicker?: boolean) => {
    const url = `${SDK_ENDPOINT}${LEDApi(channel)}`
    const params = {
        'lamp-color': color,
        'lamp-flicker': flicker
    }
    try {
        const res = await axios.post(url, {}, { params })
    } catch(err) {
        console.log(`failed changing led to ${color} at`, channel, err)
    }
}