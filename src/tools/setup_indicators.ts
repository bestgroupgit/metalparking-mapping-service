import * as moment from 'moment'

import { changeLED } from './sdk_requests'
import { LED_RED, LED_MAGNETA, LED_GREEN, LED_CYAN, LED_OFF } from '../config/sdk'

export default (current_lot: any) => {
    Object.keys(current_lot).forEach((key: string, index: number) => {
        const current_channel = current_lot[key]
        const current_channel_length = Object.keys(current_channel).length - 1
        let occupied_list: boolean[] = []
        Object.keys(current_channel).forEach((nested_key: string, nested_index: number) => {
            occupied_list.push(current_channel[nested_key].occupied)
            if(nested_index === current_channel_length) {
                const channels =  Object.keys(current_channel).map(n => current_channel[n].license_plate)
                if(occupied_list.filter(n => !n).length > 0) {
                    // console.log('changing led to green at', channels, moment().format('DD/MM/YYYY HH:mm:ss'), key)
                    // console.log(key, 'Green')
                    changeLED(key, LED_RED)
                } else {
                    // console.log('changing led to red at', channels, moment().format('DD/MM/YYYY HH:mm:ss'), key)
                    // console.log(key, 'Red')
                    changeLED(key, LED_RED)
                }
            }
        })
    })
}