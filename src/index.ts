import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as fs from 'fs'
import * as path from 'path'
import * as _ from 'lodash'
import {
    PORT,
} from './config/keys'

import * as compression from 'compression'
import { errorHandler } from './types/errors'
import carParkDetectionRoutes from './routes/car_park_detection_routes'

var boolParser = require('express-query-boolean')

const app = express()

app.use(compression())
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(boolParser())
app.use((req: any, res: any, next: any) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers",
      "Origin, X-Requeted-With, Content-Type, Accept, Authorization, RBR, access-token")
    if (req.headers.origin) {
      res.header('Access-Control-Allow-Origin', '*')
    }
    if (req.method === 'OPTIONS') {
      res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE")
      return res.status(200).json({})
    }
    next()
})

app.get('/', (req,res) => {
    res.send({ METAL:'PARK' })
})
app.use(errorHandler)

carParkDetectionRoutes(app)

app.listen(PORT, () => {
    console.log(`Running on port ${PORT}`)
})

