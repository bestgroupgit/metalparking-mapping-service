export default {
  6: {
    1: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    2: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    3: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
  },
  5: {
    18: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    19: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    20: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    4: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    5: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    6: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
  },
 1: {
    7: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    8: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    9: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    21: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    22: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    23: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
  },
  4: {
    24: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    25: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    26: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    10: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    11: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    12: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
  },
  7: {
    16: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    17: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
  },
  3: {
    13: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    14: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
    15: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
  }
}

export const lot_channel_mapping = {
  9: 1,
  3: 3,
  4: 4,
  8: 4,
  7: 7,
  6: 6,
  2: 5,
  1: 1,
  5: 5,
}
//facility_id 60


//facility_id 61
// export default {
//     1: {
//       1: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       2: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       3: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       7: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       8: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       9: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
//     },
//     3: {
//       4: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       5: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       6: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       10: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       11: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//       12: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
//     },
//   }
  
//   export const lot_channel_mapping = {
//    1: 1,
//    2: 1,
//    3: 3,
//    4: 3
//   }


// facility_id 62
// export default {
//   3: {
//     2227: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2226: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2225: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2232: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2231: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2230: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
//   },
//   4: {
//     2235: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2234: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2233: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2224: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2223: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false },
//     2222: { occupied: false, license_plate: null, start_time: null, null_count: 0, did_post: false }
//   },
// }

// export const lot_channel_mapping = {
//  1: 4,
//  3: 3,
//  2: 3,
//  4: 4
// }