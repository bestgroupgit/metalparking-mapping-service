export const PORT = process.env.PORT || '3000'
export const FACILITY_ID: number = parseInt(process.env.FACILITY_ID || '60')
export const API_ENDPOINT: string = process.env.API_ENDPOINT || 'https://metalapitesting.herokuapp.com/car-park-detection/post'
export const SDK_ENDPOINT: string = process.env.SDK_ENDPOINT || 'http://127.0.0.1:8000'
export const NVR_ID: string = process.env.NVR_ID || 'nvr-holon'