import * as moment from 'moment'
import axios from 'axios'

import { ISpaceInfo } from '../types/car_park_detection'
import lot, {lot_channel_mapping} from '../config/lot'
import setIndicators from '../tools/setup_indicators'
import { API_ENDPOINT, FACILITY_ID } from '../config/keys'
import { LED_OFF } from '../config/sdk'
import { changeLED } from '../tools/sdk_requests'

const carParkDetectionRoutes = (app: any) => {
    carParkDetection(app)
    getFacility(app)
}

let current_lot = lot
const carParkDetection = (app: any) => {
    app.post('/car-park-detection', (req: any, res: any) => {
        const facility_information: ISpaceInfo[] = req.body
        const facility_length = facility_information ? facility_information.length : 0
        const spaces = (facility_information.map(n => n.space_id))
        if(facility_length > 0) {
            let facility_information_body: ISpaceInfo[] = []
            facility_information.forEach((space_information: ISpaceInfo, i: number) => {
                const { channel, space_id, occupied, license_plate } = space_information
                const correct_channel = lot_channel_mapping[channel] 
                if(current_lot[correct_channel]) {
                    const current_space = current_lot[correct_channel][space_id]
                    if(current_space) {
                        if(current_space.occupied && occupied) { // current state is occupied and incoming state is occupied
                            const current_license_plate = current_space.license_plate
                            setSpace(current_space, true, license_plate, moment().toDate(), 0, space_id)
                            if(!current_license_plate && license_plate || license_plate !== current_license_plate) {
                                facility_information_body.push(current_space)
                            }
                        } else if(current_space.occupied && !occupied) { // current state is occupied and incoming state is vacant
                            setSpace(current_space, false, null, null ,0, space_id)
                            facility_information_body.push(current_space)
                        } else if(!current_space.occupied && occupied) { // current state is vacant and incoming state is occupied
                            setSpace(current_space, true, license_plate, moment().toDate(), 0, space_id)
                            facility_information_body.push(current_space)
                        } else if(!current_space.occupied && !occupied) { // current state is vacant and incoming state is vacant
                            if(!current_space.did_post) {
                                current_space.did_post = true
                                facility_information_body.push(current_space)
                            }
                        }
                    } else {

                    }
                } 
            })
            facility_information_body = facility_information_body.map(n => {
                if(!n.space_id) n.space_id = spaces[0]
                n.facility_id = FACILITY_ID
                n.level_id = 1
                return n
            })
            setIndicators(current_lot)
            const body = {
                facility_information: facility_information_body,
                facility_id: FACILITY_ID
            }
            axios.post(API_ENDPOINT, body)
            console.log(facility_information_body)
            res.sendStatus(200)
        } else {
            res.sendStatus(404)
        }
    })
}


setInterval(() => {
   setIndicators(current_lot)
//    changeLED(3, LED_OFF)
}, 1000)

const getFacility = (app: any) => {
    app.get('/facility', (req: any, res: any) => {
        res.send(current_lot)
    })
}

const setSpace = (space: any, occupied: any, license_plate: any, start_time: any, null_count: any, space_id: any) => {
    space.occupied = occupied
    space.license_plate = license_plate
    space.start_time = start_time
    space.null_count = null_count
    space.space_id = space_id
}

export default carParkDetectionRoutes