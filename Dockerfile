FROM node:8.11
RUN yarn global add typescript
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN yarn install
COPY . /usr/src/app
RUN tsc
EXPOSE 3000
CMD [ "node", "/usr/src/app/lib/index.js" ]
