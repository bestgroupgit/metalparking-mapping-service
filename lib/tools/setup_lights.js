"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const sdk_requests_1 = require("../tools/sdk_requests");
const sdk_1 = require("../config/sdk");
exports.default = (current_lot) => {
    Object.keys(current_lot).forEach((key, index) => {
        const current_channel = current_lot[key];
        const current_channel_length = Object.keys(current_channel).length - 1;
        let occupied_list = [];
        Object.keys(current_channel).forEach((nested_key, nested_index) => {
            occupied_list.push(current_channel[nested_key].occupied);
            if (nested_index === current_channel_length) {
                const channels = Object.keys(current_channel).map(n => current_channel[n].license_plate);
                if (occupied_list.filter(n => !n).length > 0) {
                    console.log('changing led to green at', channels, moment().format('DD/MM/YYYY HH:mm:ss'), key);
                    sdk_requests_1.changeLED(key, sdk_1.LED_MAGNETA);
                }
                else {
                    console.log('changing led to red at', channels, moment().format('DD/MM/YYYY HH:mm:ss'), key);
                    sdk_requests_1.changeLED(key, sdk_1.LED_RED);
                }
            }
        });
    });
};
//# sourceMappingURL=setup_lights.js.map