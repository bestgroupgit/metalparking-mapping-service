"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const keys_1 = require("../config/keys");
const LEDApi = (channel) => `/parking/api/nvr/${keys_1.NVR_ID}/channel/${channel}`;
exports.changeLED = (channel, color, flicker) => __awaiter(void 0, void 0, void 0, function* () {
    const url = `${keys_1.SDK_ENDPOINT}${LEDApi(channel)}`;
    const params = {
        'lamp-color': color,
        'lamp-flicker': flicker
    };
    try {
        const res = yield axios_1.default.post(url, {}, { params });
    }
    catch (err) {
        console.log(`failed changing led to ${color} at`, channel, err);
    }
});
//# sourceMappingURL=sdk_requests.js.map