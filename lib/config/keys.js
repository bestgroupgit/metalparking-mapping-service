"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PORT = process.env.PORT || '3000';
exports.FACILITY_ID = parseInt(process.env.FACILITY_ID || '60');
exports.API_ENDPOINT = process.env.API_ENDPOINT || 'https://metalapitesting.herokuapp.com/car-park-detection/post';
exports.SDK_ENDPOINT = process.env.SDK_ENDPOINT || 'http://127.0.0.1:8000';
exports.NVR_ID = process.env.NVR_ID || 'nvr-holon';
//# sourceMappingURL=keys.js.map