"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LED_RED = 'red';
exports.LED_WHITE = 'white';
exports.LED_CYAN = 'cyan';
exports.LED_MAGNETA = 'magenta';
exports.LED_YELLOW = 'yellow';
exports.LED_GREEN = 'green';
exports.LED_OFF = 'off';
//# sourceMappingURL=sdk.js.map