"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ValidationError extends Error {
    constructor(message) {
        super(message);
    }
}
exports.ValidationError = ValidationError;
function errorHandler(err, req, res, next) {
    console.error(err);
    if (err instanceof ValidationError) {
        res.send({ ok: false, result: err.message });
    }
    else {
        res.status(500);
        res.send({ ok: false, result: '500 Internal Server Error' });
    }
}
exports.errorHandler = errorHandler;
//# sourceMappingURL=errors.js.map