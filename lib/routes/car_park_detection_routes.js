"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const axios_1 = require("axios");
const lot_1 = require("../config/lot");
const setup_indicators_1 = require("../tools/setup_indicators");
const keys_1 = require("../config/keys");
const carParkDetectionRoutes = (app) => {
    carParkDetection(app);
    getFacility(app);
};
let current_lot = lot_1.default;
const carParkDetection = (app) => {
    app.post('/car-park-detection', (req, res) => {
        const facility_information = req.body;
        const facility_length = facility_information ? facility_information.length : 0;
        const spaces = (facility_information.map(n => n.space_id));
        if (facility_length > 0) {
            let facility_information_body = [];
            facility_information.forEach((space_information, i) => {
                const { channel, space_id, occupied, license_plate } = space_information;
                const correct_channel = lot_1.lot_channel_mapping[channel];
                if (current_lot[correct_channel]) {
                    const current_space = current_lot[correct_channel][space_id];
                    if (current_space) {
                        if (current_space.occupied && occupied) { // current state is occupied and incoming state is occupied
                            const current_license_plate = current_space.license_plate;
                            setSpace(current_space, true, license_plate, moment().toDate(), 0, space_id);
                            if (!current_license_plate && license_plate || license_plate !== current_license_plate) {
                                facility_information_body.push(current_space);
                            }
                        }
                        else if (current_space.occupied && !occupied) { // current state is occupied and incoming state is vacant
                            setSpace(current_space, false, null, null, 0, space_id);
                            facility_information_body.push(current_space);
                        }
                        else if (!current_space.occupied && occupied) { // current state is vacant and incoming state is occupied
                            setSpace(current_space, true, license_plate, moment().toDate(), 0, space_id);
                            facility_information_body.push(current_space);
                        }
                        else if (!current_space.occupied && !occupied) { // current state is vacant and incoming state is vacant
                            if (!current_space.did_post) {
                                current_space.did_post = true;
                                facility_information_body.push(current_space);
                            }
                        }
                    }
                    else {
                    }
                }
            });
            facility_information_body = facility_information_body.map(n => {
                if (!n.space_id)
                    n.space_id = spaces[0];
                n.facility_id = keys_1.FACILITY_ID;
                n.level_id = 1;
                return n;
            });
            setup_indicators_1.default(current_lot);
            const body = {
                facility_information: facility_information_body,
                facility_id: keys_1.FACILITY_ID
            };
            axios_1.default.post(keys_1.API_ENDPOINT, body);
            console.log(facility_information_body);
            res.sendStatus(200);
        }
        else {
            res.sendStatus(404);
        }
    });
};
setInterval(() => {
    setup_indicators_1.default(current_lot);
    //    changeLED(3, LED_OFF)
}, 1000);
const getFacility = (app) => {
    app.get('/facility', (req, res) => {
        res.send(current_lot);
    });
};
const setSpace = (space, occupied, license_plate, start_time, null_count, space_id) => {
    space.occupied = occupied;
    space.license_plate = license_plate;
    space.start_time = start_time;
    space.null_count = null_count;
    space.space_id = space_id;
};
exports.default = carParkDetectionRoutes;
//# sourceMappingURL=car_park_detection_routes.js.map