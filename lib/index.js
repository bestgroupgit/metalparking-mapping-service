"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const keys_1 = require("./config/keys");
const compression = require("compression");
const errors_1 = require("./types/errors");
const car_park_detection_routes_1 = require("./routes/car_park_detection_routes");
var boolParser = require('express-query-boolean');
const app = express();
app.use(compression());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(boolParser());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requeted-With, Content-Type, Accept, Authorization, RBR, access-token");
    if (req.headers.origin) {
        res.header('Access-Control-Allow-Origin', '*');
    }
    if (req.method === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
        return res.status(200).json({});
    }
    next();
});
app.get('/', (req, res) => {
    res.send({ METAL: 'PARK' });
});
app.use(errors_1.errorHandler);
car_park_detection_routes_1.default(app);
app.listen(keys_1.PORT, () => {
    console.log(`Running on port ${keys_1.PORT}`);
});
//# sourceMappingURL=index.js.map